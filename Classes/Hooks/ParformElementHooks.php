<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace Parousia\Parform\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Validation\Error;
use TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface;
use TYPO3\CMS\Form\Domain\Model\Renderable\RootRenderableInterface;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;
use TYPO3\CMS\Form\Service\TranslationService;
use Parousia\Parform\Domain\Repository\FormRepository;

/**
 * Class FormHooks
 *
 * @package Parousia\Parform\Hooks
 */
class ParformElementHooks
{
    protected $FormRepository;

    /**
     * This hook is invoked by the FormRuntime for each form element
     * **after** a form page was submitted but **before** values are
     * property-mapped, validated and pushed within the FormRuntime's `FormState`.
     *
     * @param FormRuntime $formRuntime
     * @param RenderableInterface $renderable
     * @param mixed $elementValue submitted value of the element *before post processing*
     * @param array $requestArguments submitted raw request values
     * @return mixed
     * @see FormRuntime::mapAndValidate()
     * @internal
     */
    public function afterSubmit(FormRuntime $formRuntime, RenderableInterface $renderable, $elementValue, array $requestArguments = [])
    {
    //    if ($renderable->getType() === 'AdvancedPassword') {
    //        if ($elementValue['password'] !== $elementValue['confirmation']) {
		if (is_array($requestArguments))
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SearchForm arguments: '.urldecode(http_build_query($requestArguments,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Hooks/debug.txt');
		if ($renderable->getIdentifier()=='text-1')
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ParformElementHooks identifier: '.$renderable->getIdentifier().'; label:'.$renderable->getLabel().'; class:'.$renderable->getRendererClassName()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Hooks/debug.txt');
			$geboortedatum=$requestArguments['text-6'];
			$postcode=$requestArguments['text-4'];
			$huisnummer=$requestArguments['text-7'];
			$search=['text-6'=> $geboortedatum,'text-4'=>$postcode,'text-7'=>$huisnummer];
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ParformElementHooks geboortedatum: '.$geboortedatum.'; postcode:'.$postcode.'; huisnummer:'.$huisnummer."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Hooks/debug.txt');
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ParformElementHooks search: '.urldecode(http_build_query($search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Hooks/debug.txt');
			// check if already request given:
			$this->FormRepository=GeneralUtility::makeInstance(FormRepository::class);
			if ($this->FormRepository->findFormSelection($search)>0)
			{
                $processingRule = $renderable->getRootForm()->getProcessingRule($renderable->getIdentifier());
                $processingRule->getProcessingMessages()->addError(
                    GeneralUtility::makeInstance(
						Error::class,'Je hebt al een aanvraag voor aansluiting ingediend',1556283178
                    )
                );
			}
			if ($this->FormRepository->findPersoonSelection($search))
			{
                $processingRule = $renderable->getRootForm()->getProcessingRule($renderable->getIdentifier());
                $processingRule->getProcessingMessages()->addError(
                    GeneralUtility::makeInstance(
						Error::class,'Je bent al aangesloten bij ons gemeente',1556283179
                    )
                );
			}
        }
     //       $elementValue = $elementValue['password'];
     //   }

        return $elementValue;
    }

}
