<?php

namespace Parousia\Parform\Domain\Repository;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Core\Utility\GeneralUtility;

ini_set("display_errors",1);
ini_set("log_errors",1);

/**
 * Class FormRepository
 *
 * @package Parousia\Parform\Domain\Repository
 *
 * return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class FormRepository 
{

	protected $db;
 /**
     * findFormSelection function 
     *
     * param array  $search
     *
     * return nbr found
     */

	public function findFormSelection(array $search = null)
	{
		churchpersreg_div::connectdb($this->db);
		$where='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findFormSelection search: '.urldecode(http_build_query($search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Hooks/debug.txt');
		foreach ($search as $key => $value)
		{
			if (!empty($where))$where.=' and ';
			$where.='result->\'$."'.$key.'"\'="'.$value.'"';
		}
		$statement="select count(*) as nbr from tx_formtodatabase_domain_model_formresult where ".$where;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findFormSelection statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Domain/Repository/debug.txt');
		$result=$this->db->query($statement) or die("Can't perform Query");	
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$nbr=$row['nbr'];
		return $nbr;
    }

		
 /**
     * findPersoonSelection function 
     *
     * param array  $search
     *
     * return nbr found
     */

	public function findPersoonSelection(array $search = null)
	{
		churchpersreg_div::connectdb($this->db);
		$aSearch=array();
		foreach ($search as $key => $value)
		{
			if ($key=="text-6")	$aSearch['geboortedatum']= (new \DateTime($value))->format("Y-m-d");
			elseif ($key=="text-4") $aSearch['postcode']=$value;
			elseif ($key=="text-7") $aSearch['huisnummer']=$value;
		}
		$zoekargument=$this->ComposeSearch($aSearch);
		$statement = 'SELECT count(*) as nbr'.
		' FROM persoon as tp,adres as ta'.
		' where '.$zoekargument;

//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findPersoonSelection statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Domain/Repository/debug.txt');
		$result=$this->db->query($statement) or die("Can't perform Query");	
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$nbr=$row['nbr'];
		return $nbr;
    }

		/**
	 * Method 'ComposeSearch' for the 'churchpersreg' extension.
 	*
	 * param searchfield: keyword required persons ({$Zoekveld,$Zoekveld_1,$Zoekveld_2,$Zoekveld_3,$Zoekveld_4,$Zoekveld_5,$Zoekveld_6})
	 * returns composed sql search parameter
	 */
	function ComposeSearch(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//			$this->ErrMsg.="Compose zoekveld 1:".$Zoekveld[1].",2:".$Zoekveld[2].",3:".$Zoekveld[3].",4:".$Zoekveld[4].",5:".$Zoekveld[5].",6:".$Zoekveld[6].",7:".$Zoekveld[7];
		$zoekargument="";
		$connection=\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getConnectionForTable('string');
	/*	if (!empty($Zoekveld[7])){
			$zoekargument .=  "(tp.uid = '" . addslashes($Zoekveld[7]) ."') AND ";}		*/
		if ($search['geboortedatum']<>""){
			$zoekargument .=  'tp.geboortedatum = "'.$search['geboortedatum'].'" AND ';}
		if ($search['postcode']<>""){ 
			$zoekargument .=  "ta.postcode LIKE '%" . addslashes($search['postcode']) ."%' AND ";}
		if ($search['huisnummer']<>""){ 
			$search['huisnummer']=trim($search['huisnummer']);
			$zoekargument .=  "ta.huisnummer LIKE '%" . addslashes($search['huisnummer']) ."%' AND ";
		}

		$zoekargument.=' AES_DECRYPT(tp.id_adres,@password)=ta.uid and tp.deleted=0 and ta.deleted=0';
//		$this->ErrMsg.=",br>Zoekargument:".$zoekargument;
		return $zoekargument;
	}
	
	
}