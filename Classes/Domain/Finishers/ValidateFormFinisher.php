<?php
/**
 * This file is part of the "form_to_database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Parousia\Parform\Domain\Finishers;

use Parousia\Parform\Domain\Repository\FormRepository;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher;
use TYPO3\CMS\Form\Domain\Finishers\FinisherContext;
use TYPO3\CMS\Form\Domain\Finishers\FlashMessageFinisher;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FormToDatabaseFinisher
 *
 * @package Parousia\Parform\Domain\Finishers
 */
class ValidateFormFinisher extends AbstractFinisher
{

    /**
     * The FormRepository
     *
     * @var FormRepository
     */
    protected $formRepository;

	
    public function __construct()
	{
        $this->FormRepository = GeneralUtility::makeInstance(FormRepository::class);
    }

	

    /**
     * Injects the FormRepository
     *
     * @param FormRepository $formRepository
     */
/*    public function injectFormRepository(FormRepository $formRepository): void
    {
        $this->FormRepository = $formRepository;
    } */

    /**
     * Writes the form-result into the database
     *
     * @throws IllegalObjectTypeException
     */
    protected function executeInternal(): void
    {
		$formValues = [];
		foreach ($this->finisherContext->getFormValues() as $fieldName => $fieldValue) {
        	$formValues[$fieldName] = $fieldValue;
		}
		$geboortedatum=$formValues['text-6'];
		$postcode=$formValues['text-4'];
		$huisnummer=$formValues['text-7'];
		$search=['text-6'=> $geboortedatum,'text-4'=>$postcode,'text-7'=>$huisnummer];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ValidateFormFinisher : '.urldecode(http_build_query($search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parform/Classes/Hooks/debug.txt');
		if ($this->FormRepository->findFormSelection($search)>0)
		{
			$this->finisherContext->cancel();
		}
    }
}
