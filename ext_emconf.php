<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Form parousia',
    'description' => 'Contains definitions of parousia forms.',
    'category' => 'fe',
    'author' => 'Karel Kuijpers',
    'author_email' => 'karelkuijpers@gmail.come',
    'state' => 'stable',
    'version' => '11.0.2',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-11.5.99',
            'form' => '2.0.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
