<?php
defined('TYPO3') or die();
call_user_func(function () {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['afterSubmit'][] = \Parousia\Parform\Hooks\ParformElementHooks::class;
});
